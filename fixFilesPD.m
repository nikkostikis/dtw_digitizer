function fixFilesPD

%iterate through the DTWMATSPD directory
dirname = 'DTWMATSPD';
D = dir(dirname);
count = 0;
for i = 3:length(D)
    count = count +1;
    filename = D(i).name;
    fprintf('%d --> %s\n', count, filename);
    data = load(strcat(dirname, '/', filename));
    save('-mat7-binary', strcat('DTWMATSPD_fixed/',filename), 'data');
end