function calcDTWmat_OH
% Claculate DTW d's to a different mat for each signal
% The idea is to be able to use the mats before the execution ends

load 'ParsedData/signals_OH.mat'
load 'ParsedData/fileNames_OH.mat'


numOH = size(fileNames_OH); %count of OH volunteers +1 header
numOH = numOH(1) - 1 ; % -1 due to headers


% numPD = 7; % Testing files
% numOH = 3; % Testing files


logFid=fopen('Progress_Log_OH.txt','w+');
fprintf(logFid,'\nEntered OH DTW calculations\n'); % Initial entry to logFile
fclose(logFid);

for i = 2 : numOH + 1    
    s1 = signals_OH{i, 1};
    tempD = zeros(1,numOH);
    for j = 2 : numOH + 1
       s2 = signals_OH{j, 1};
       tempD(j-1) = dtw(s1, s2);
    end
    % save tempD to an approriate variable
    eval(strcat('DTWmat_OH_fnum_', num2str(i-1), '= tempD;'));
    % save the variable to file
    save( strcat('DTWMATSOH/DTWmat_OH_fnum_', num2str(i-1), '.mat'), strcat('DTWmat_OH_fnum_', num2str(i-1)) );
    logFid=fopen('Progress_Log_OH.txt','w+');
    fprintf(logFid, 'Completed all for OH signal: %d \n', i-1);
    fclose(logFid);
end