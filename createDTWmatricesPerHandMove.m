function createDTWmatricesPerHandMove

load 'ParsedData/fileNames_OH.mat';
% load 'ParsedData/fileNames_PD.mat';
load 'DTWMatrix_OH.mat';
% load 'DTWMatrix_PD.mat';

% keep the columns with the hand and move codes
moveVec = fileNames_OH(:,3);
moveVec = moveVec(2:end, 1);
handVec = fileNames_OH(:,4);
handVec = handVec(2:end, 1);

% find indices for hand and move
indHandR = find([handVec{:,1}] == 'R');
indHandL = find([handVec{:,1}] == 'L');
indMoveE = find([moveVec{:,1}] == 'E');
indMoveF = find([moveVec{:,1}] == 'F');

% intersect indices
indRE = intersect(indHandR, indMoveE);
indRF = intersect(indHandR, indMoveF);
indLE = intersect(indHandL, indMoveE);
indLF = intersect(indHandL, indMoveF);

% create DTW matrices per hand and move
dtwRE = DTWMatrix(indRE, indRE);
dtwRF = DTWMatrix(indRF, indRF);
dtwLE = DTWMatrix(indLE, indLE);
dtwLF = DTWMatrix(indLF, indLF);

% standardize values of matrices


keyboard;