% receives a matrix and replaces the zeros
% of the main diagonal with inf values
function B = zeros2inf(A)

B = A;
B(logical(eye(size(B)))) = inf;