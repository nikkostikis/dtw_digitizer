function findMins

matrixStruct = load('DTWMatrix_OH.mat');
data = matrixStruct.DTWMatrix;
numrows = size(data);

% set the diagonal elements to inf
% pass an inverted Identity matrix as indices to DTWMatrix
% data(logical(eye(size(data)))) = inf;

% mins of each column
mins = min(data);
% cell to hold the indices of the min inside each column
imin = cell(1, numrows(1));

for j = 1:numrows(1)
    imin{j} = find(data(:,j) == mins(j));
end
indArray = zeros(1, 1);
for i = 1:numrows(1)
    tempArray = imin{1,i}';
    indArray = [indArray tempArray];
end
indArray = indArray(1, 2:end);
unIndArray = unique(indArray);
histOut = hist(indArray, unIndArray);

% histogram(histOut, length(unique(indArray)));
bar(histOut);


