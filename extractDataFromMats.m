function extractDataFromMats


DTWMatrix = zeros(804, 804);
dirname = 'DTWMATSOH_fixed'
D = dir(dirname);
for i = 3:length(D)
    fileName = D(i).name;
    % filename is something like DTWmat_OH_fnum_10.mat
    data = load(strcat(dirname, '/', fileName));
    
    % extract the name of the file without the '.mat' because
    % it is the field of the struct containing the DTW data
    fieldName = strsplit(fileName, '.');
    % exttract the DTW data from the field of the loaded mat
    eval(strcat('raw = data.data.', fieldName{1}))
    
    fprintf('loaded from: %s...\n', strcat(dirname, '/', fileName));
    
    % I need to put the DTW data in the correct row to be able
    % to recognize the files later. I need to extract the number
    % of the file - they are not read in order
    fileNameComponents = regexp(fileName, '[_.]+','split');
    fileNum = str2num(fileNameComponents{4});
    fprintf('gonna put it in row %d of %dx%d array ...\n', fileNum, length(D)-2, length(D)-2);
    DTWMatrix(fileNum, :) = raw;
%     pause;
end
% pause;
save('DTWMatrix_OH.mat', 'DTWMatrix');
