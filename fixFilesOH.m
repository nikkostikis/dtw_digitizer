function fixFilesOH

%iterate through the DTWMATSOH directory
dirname = 'DTWMATSOH';
D = dir(dirname);
count = 0;
for i = 3:length(D)
    count = count +1;
    filename = D(i).name;
    fprintf('%d --> %s\n', count, filename);
    data = load(strcat(dirname, '/', filename));
    save('-mat7-binary', strcat('DTWMATSOH_fixed/',filename), 'data');
end