The code in this repository is used to create the files in the folders and manipulate them. 

The calcs and the fixxers ran on a server (Octave on Linux) and the final product is in the <*_fixed?> folders.

The extractors load the files and create NxN (where N = count of files in folders) matrices,
where each file (row) has its DTW distance from each other file (column).