function calcDTWmat_PD_reverse 


% Claculate DTW d's to a different mat for each signal
% The idea is to be able to use the mats before the execution ends
% Deal with the signals from the last to the first

load 'ParsedData/signals_PD.mat'
load 'ParsedData/fileNames_PD.mat'


numPD = size(fileNames_PD); %count of PD volunteers +1 header
numPD = numPD(1) - 1 ; % -1 due to headers


% numPD = 7; % Testing files
% numPD = 3; % Testing files


logFid=fopen('Progress_Log_PD_Rev.txt','w+');
fprintf(logFid,'\nEntered PD DTW calculations\n'); % Initial entry to logFile
fclose(logFid);

for i = numPD + 1 : -1 : 2     
    s1 = signals_PD{i, 1};
    tempD = zeros(1,numPD);
    for j = 2 : numPD + 1
       s2 = signals_PD{j, 1};
       tempD(j-1) = dtw(s1, s2);
    end
    % save tempD to an approriate variable
    eval(strcat('DTWmat_PD_fnum_', num2str(i-1), '= tempD;'));
    % save the variable to file
    save( strcat('DTWMATSPDREV/DTWmat_PD_fnum_', num2str(i-1), '.mat'), strcat('DTWmat_PD_fnum_', num2str(i-1)) );
    logFid=fopen('Progress_Log_PD_Rev.txt','w+');
    fprintf(logFid, 'Completed all for PD signal: %d \n', i-1);
    fclose(logFid);
end
